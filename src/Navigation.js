import { StackNavigator } from 'react-navigation';
import Welcome from './components/Welcome';
import Main from './components/Main';
import FormLogin from './components/FormLogin';
import FormSignUp from './components/FormSignUp';
import AddContact from './components/AddContact';
import Chat from './components/Chat';

const Navigation = StackNavigator({
  Welcome: {
    screen: Welcome,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Main: {
    screen: Main,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  FormLogin: {
    screen: FormLogin,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  FormSignUp: {
    screen: FormSignUp,
    navigationOptions: ({ navigation }) => ({
      title: 'Cadastro'
    })
  },
  AddContact: {
    screen: AddContact,
    navigationOptions: ({ navigation }) => ({
      title: 'Adicionar Contato'
    })
  },
  Chat: {
    screen: Chat
  },
}, {
  initialRouteName: 'FormLogin',
  mode: 'card',
  navigationOptions: {
    headerStyle: { backgroundColor: '#115E54' },
    headerTintColor: '#ffffff'
  }
  // headerMode: 'none'
});

export default Navigation;
