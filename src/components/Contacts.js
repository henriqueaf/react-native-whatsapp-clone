import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';

import { userContactsFetch } from '../actions/AppActions';

class Contacts extends Component {
  componentWillMount() {
    this.props.userContactsFetch();
  }

  keyExtractor = (item, index) => (item.uid);

  renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => this.props.navigate('Chat', { contactName: item.name, contactEmail: item.email })}>
      <View style={{ flex: 1, padding: 20, borderBottomWidth: 1, borderColor: '#CCC' }}>
        <Text style={{ color: '#000', fontSize: 25 }}>{item.name}</Text>
        <Text style={{ color: '#808080', fontSize: 18 }}>{item.email}</Text>
      </View>
    </TouchableOpacity>
  );

  render() {
    return (
      <FlatList
        data={this.props.contactsList}
        renderItem={this.renderItem}
        keyExtractor={this.keyExtractor}
      />
    );
  }
}

const mapStateToProps = (state) => {
  const contactsList = _.map(state.UserContactsListReducer.contactsList, (val, uid) => (
    { ...val, uid }
  ));

  return ({
    contactsList
  });
};

export default connect(mapStateToProps, { userContactsFetch })(Contacts);
