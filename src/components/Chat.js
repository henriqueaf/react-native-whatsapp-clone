import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, FlatList, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';

import { changeMessage, sendMessage, userChatFetch } from '../actions/AppActions';

const sendMessageImage = require('../images/enviar_mensagem.png');

class Chat extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.contactName
  });

  componentWillMount() {
    this.props.userChatFetch(this.props.navigation.state.params.contactEmail);
  }

  deliverMessage() {
    const message = this.props.message;
    const { contactName, contactEmail } = this.props.navigation.state.params;

    this.props.sendMessage(message, contactName, contactEmail);
    this.refs.MessageInput.clear();
  }

  keyExtractor = (item, index) => (item.uid);

  renderItem = ({ item }) => {
    let textContainerAlign = 'flex-start';
    let textContainerMargin = { marginRight: 40 };
    let textBackgroundColor = 'white';

    if (item.type === 'e') {
      textContainerAlign = 'flex-end';
      textBackgroundColor = '#D2FDC6';
      textContainerMargin = { marginLeft: 40 };
    }

    return (
      <View
        style={[
          styles.chatTextContainer,
          { alignItems: textContainerAlign },
          textContainerMargin
        ]}
      >
        <Text style={[styles.chatText, { backgroundColor: textBackgroundColor }]}>
          {item.message}
        </Text>
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#EEE4DC', padding: 10 }}>
        <View style={{ flex: 1, paddingBottom: 10 }}>
          <FlatList
            data={this.props.chat}
            renderItem={this.renderItem}
            keyExtractor={this.keyExtractor}
          />
        </View>

        <View style={{ flexDirection: 'row', height: 60 }}>
          <TextInput
            ref='MessageInput'
            style={{ flex: 4, backgroundColor: '#fff', fontSize: 18 }}
            onChangeText={(text) => this.props.changeMessage(text)}
            numberOfLines={1}
            autoCapitalize='sentences'
          />

          <TouchableOpacity onPress={() => this.deliverMessage()}>
            <Image source={sendMessageImage} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  chatTextContainer: {
    marginVertical: 4
  },
  chatText: {
    color: 'black',
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 7,
    borderRadius: 4,
    elevation: 1
  }
});

const mapStateToProps = (state) => {
  const chat = _.map(state.ChatListReducer, (val, uid) => (
    { ...val, uid }
  ));

  return ({
    chat,
    message: state.AppReducer.message
  });
};

export default connect(mapStateToProps, { changeMessage, sendMessage, userChatFetch })(Chat);
