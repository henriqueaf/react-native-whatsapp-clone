import React from 'react';
import {
  View,
  Text,
  Button,
  Image
} from 'react-native';

const logo = require('../images/logo.png');
const backgroundImage = require('../images/bg.png');

const welcome = (props) => {
  const { navigate } = props.navigation;

  return (
    <Image style={{ flex: 1, width: null }} source={backgroundImage}>
      <View style={{ flex: 1, padding: 15 }}>
        <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 20, color: 'white' }}>Seja bem-vindo!</Text>
          <Image source={logo} />
        </View>
        <View style={{ flex: 1 }}>
          <Button title='Fazer Login' onPress={() => props.navigation.navigate('FormLogin')} />
        </View>
      </View>
    </Image>
  );
};

export default welcome;
