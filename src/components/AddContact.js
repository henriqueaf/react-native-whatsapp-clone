import React, { Component } from 'react';
import { View, TextInput, Button, Text } from 'react-native';
import { connect } from 'react-redux';

import { modifyAddContactEmail, addContact } from '../actions/AppActions';

class AddContact extends Component {
  renderAddContact() {
    if (this.props.addContactResultSuccess) {
      return (
        <View style={{ flex: 1 }}>
          <Text>Usuário adicionado com sucesso!</Text>
        </View>
      );
    }

    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <TextInput
            placeholder='E-mail'
            style={{ fontSize: 20, height: 45 }}
            onChangeText={(text) => this.props.modifyAddContactEmail(text)}
            value={this.props.addContactEmail}
          />
        </View>

        <View style={{ flex: 1 }}>
          <Button
            title="Adicionar"
            color="#115E54"
            onPress={() => this.props.addContact(this.props.addContactEmail)}
          />

          <Text style={{ color: '#ff0000', fontSize: 20, textAlign: 'center' }}>
            {this.props.addContactResultError}
          </Text>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', padding: 20 }}>
        {this.renderAddContact()}
      </View>
    );
  }
} 

const mapStateToProps = (state) => ({
  addContactEmail: state.AppReducer.addContactEmail,
  addContactResultError: state.AppReducer.addContactResultError,
  addContactResultSuccess: state.AppReducer.addContactResultSuccess,
});

export default connect(mapStateToProps, { modifyAddContactEmail, addContact })(AddContact);
