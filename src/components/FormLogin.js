import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';
import { connect } from 'react-redux';

import {
  modifyEmail,
  modifyPassword,
  authenticateUser,
  checkUserSignedIn
} from '../actions/AuthenticationActions';

const backgroundImage = require('../images/bg.png');

class FormLogin extends Component {
  componentWillMount() {
    this.props.checkUserSignedIn(this.props.navigation);
  }

  authenticateUser() {
    const { email, password } = this.props;
    const { navigate } = this.props.navigation;
    this.props.authenticateUser({ email, password, navigate });
  }

  renderSignInButton() {
    if (this.props.loadingLogin) {
      return (
        <ActivityIndicator size="large" />
      );
    }

    return (
      <Button
        title="Acessar"
        color="#115E54"
        style={styles.loginButton}
        onPress={() => this.authenticateUser()}
      />
    );
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <Image style={{ flex: 1, width: null }} source={backgroundImage}>
        <View style={styles.main}>
          <View style={styles.header}>
            <Text style={styles.titleText}>Whatsapp Clone</Text>
          </View>

          <View style={styles.body}>
            <TextInput
              style={styles.input}
              placeholder="E-mail"
              placeholderTextColor='#ffffff'
              value={this.props.email}
              onChangeText={(text) => this.props.modifyEmail(text)}
            />
            <TextInput
              style={styles.input}
              placeholder="Senha"
              placeholderTextColor='#ffffff'
              value={this.props.password}
              secureTextEntry
              onChangeText={(text) => this.props.modifyPassword(text)}
            />

            <Text style={{ fontSize: 18, color: 'red', textAlign: 'center' }}>
              {this.props.authenticateError}
            </Text>

            <TouchableOpacity onPress={() => navigate('FormSignUp')}>
              <Text style={styles.signUpText}>Ainda não tem cadastro? Cadastre-se!</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.footer}>
            {this.renderSignInButton()}
          </View>
        </View>
      </Image>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 10
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  body: {
    flex: 2
  },
  footer: {
    flex: 2
  },
  titleText: {
    fontSize: 25,
    color: '#ffffff'
  },
  input: {
    fontSize: 20,
    height: 45
  },
  signUpText: {
    fontSize: 20,
    color: '#ffffff',
    textAlign: 'center'
  }
});

const mapStateToProps = (state) => ({
  email: state.AuthenticationReducer.email,
  password: state.AuthenticationReducer.password,
  authenticateError: state.AuthenticationReducer.authenticateError,
  loadingLogin: state.AuthenticationReducer.loadingLogin
});

export default connect(mapStateToProps, {
  modifyEmail,
  modifyPassword,
  authenticateUser,
  checkUserSignedIn
})(FormLogin);
