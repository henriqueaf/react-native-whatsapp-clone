import React from 'react';
import { View, Text, StatusBar, Image, TouchableOpacity } from 'react-native';
import { TabBar } from 'react-native-tab-view';
import { connect } from 'react-redux';

import { enableAddContact } from '../actions/AppActions';

const addContactImage = require('../images/adicionar-contato.png');

const TabBarMenu = (props) => (
  <View style={{ backgroundColor: '#115E54', elevation: 4, marginBottom: 6 }}>
    <StatusBar backgroundColor="#114D44" />

    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
      <View style={{ height: 50, justifyContent: 'center' }}>
        <Text style={{ color: '#fff', fontSize: 20, marginLeft: 20 }}>WhatsApp Clone</Text>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'center', width: 75, justifyContent: 'space-between', marginRight: 20 }}>
        <View>
          <TouchableOpacity onPress={() => {
              props.enableAddContact();
              props.navigate('AddContact');
            }}
          >
            <Image source={addContactImage} />
          </TouchableOpacity>
        </View>

        <View>
          <Text style={{ fontSize: 18, color: '#fff' }}>Sair</Text>
        </View>
      </View>
    </View>

    <TabBar {...props} style={{ backgroundColor: '#115E54', elevation: 0 }} />
  </View>
);

export default connect(null, { enableAddContact })(TabBarMenu);
