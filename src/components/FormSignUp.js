import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  TextInput,
  StyleSheet,
  Button,
  Image,
  Text
} from 'react-native';
import { connect } from 'react-redux';

import {
  modifyEmail,
  modifyPassword,
  modifyName,
  signUpUser
} from '../actions/AuthenticationActions';

const backgroundImage = require('../images/bg.png');

class FormSignUp extends Component {
  signUp() {
    const { name, email, password, navigation } = this.props;

    this.props.signUpUser({ name, email, password, navigate: navigation.navigate });
  }

  renderSignUpButton() {
    if (this.props.loadingSignUp) {
      return (
        <ActivityIndicator size="large" />
      );
    }

    return (
      <Button
        title="Cadastrar"
        color="#115E54"
        style={styles.loginButton}
        onPress={() => this.signUp()}
      />
    );
  }

  render() {
    return (
      <Image style={{ flex: 1, width: null }} source={backgroundImage}>
        <View style={styles.main}>
          <View style={styles.body}>
            <TextInput
              style={styles.input}
              placeholder="Nome"
              placeholderTextColor='#ffffff'
              value={this.props.name}
              onChangeText={(text) => this.props.modifyName(text)}
            />
            <TextInput
              style={styles.input}
              placeholder="E-mail"
              placeholderTextColor='#ffffff'
              value={this.props.email}
              onChangeText={(text) => this.props.modifyEmail(text)}
            />
            <TextInput
              style={styles.input}
              placeholder="Senha"
              placeholderTextColor='#ffffff'
              value={this.props.password}
              secureTextEntry
              onChangeText={(text) => this.props.modifyPassword(text)}
            />

            <Text style={{ fontSize: 18, color: 'red' }}>{this.props.signUpError}</Text>
          </View>

          <View style={styles.footer}>
            {this.renderSignUpButton()}
          </View>
        </View>
      </Image>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 10
  },
  body: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  footer: {
    flex: 1
  },
  input: {
    fontSize: 20,
    height: 45,
    width: '100%'
  }
});

const mapStateToProps = (state) => ({
  name: state.AuthenticationReducer.name,
  email: state.AuthenticationReducer.email,
  password: state.AuthenticationReducer.password,
  signUpError: state.AuthenticationReducer.signUpError,
  loadingSignUp: state.AuthenticationReducer.loadingSignUp
});

export default connect(mapStateToProps,
  {
    modifyEmail,
    modifyPassword,
    modifyName,
    signUpUser
  })(FormSignUp);
