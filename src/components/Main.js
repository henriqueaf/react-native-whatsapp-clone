import React, { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { TabViewAnimated } from 'react-native-tab-view';

import TabBarMenu from './TabBarMenu';
import Chats from './Chats';
import Contacts from './Contacts';

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

export default class Main extends Component {
  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'Conversas' },
      { key: 'second', title: 'Contatos' },
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => <TabBarMenu {...props} navigate={this.props.navigation.navigate} />;

  _renderScene = ({ route }) => {
    switch (route.key) {
    case 'first':
      return <Chats />;
    case 'second':
      return <Contacts navigate={this.props.navigation.navigate} />;
    default:
      return null;
    }
  }

  render() {
    return (
      <TabViewAnimated
        style={styles.container}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
        initialLayout={initialLayout}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
