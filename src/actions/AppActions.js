import firebase from 'firebase';
import b64 from 'base-64';
import _ from 'lodash';

import {
  MODIFY_ADD_CONTACT_EMAIL,
  ADD_CONTACT_ERROR,
  ADD_CONTACT_SUCCESS,
  USER_CONTACTS_LIST,
  CHANGE_MESSAGE,
  SEND_MESSAGE_SUCCESS,
  USER_CHAT_LIST_FETCH
} from '../actions/types';

export const modifyAddContactEmail = (text) => (
  {
    type: MODIFY_ADD_CONTACT_EMAIL,
    payload: text
  }
);

export const addContact = (email) => {
  return (dispatch) => {
    const emailBase64 = b64.encode(email);

    firebase.database().ref(`/contacts/${emailBase64}`)
      .once('value')
      .then((snapshot) => {
        if (snapshot.val()) {
          const userData = _.first(_.values(snapshot.val()));
          const { currentUser } = firebase.auth();
          const currentUserEmailB64 = b64.encode(currentUser.email);

          firebase.database().ref(`/user_contacts/${currentUserEmailB64}`)
            .push({ email, name: userData.name })
            .then(() => addContactSuccess(dispatch))
            .catch((error) => addContactError(error.message, dispatch));
        } else {
          addContactError('E-mail informado não corresponde a um usuário válido!', dispatch);
        }
      });
  };
};

const addContactSuccess = (dispatch) => (
  dispatch({
    type: ADD_CONTACT_SUCCESS,
    payload: true
  })
);

const addContactError = (error, dispatch) => (
  dispatch({
    type: ADD_CONTACT_ERROR,
    payload: error
  })
);

export const enableAddContact = () => (
  {
    type: ADD_CONTACT_SUCCESS,
    payload: false
  }
);

export const userContactsFetch = () => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    const currentUserEmailB64 = b64.encode(currentUser.email);
    firebase.database().ref(`/user_contacts/${currentUserEmailB64}`)
      .on('value', (snapshot) => {
        dispatch({
          type: USER_CONTACTS_LIST,
          payload: snapshot.val()
        });
      });
  };
};

export const changeMessage = (text) => (
  {
    type: CHANGE_MESSAGE,
    payload: text
  }
);

export const sendMessage = (message, contactName, contactEmail) => {
  const { currentUser } = firebase.auth();
  const currentUserEmailB64 = b64.encode(currentUser.email);
  const contactEmailB64 = b64.encode(contactEmail);

  return (dispatch) => {
    firebase.database().ref(`/messages/${currentUserEmailB64}/${contactEmailB64}`)
      .push({ message, type: 'e' })
      .then(() => {
        firebase.database().ref(`/messages/${contactEmailB64}/${currentUserEmailB64}`)
          .push({ message, type: 'r' })
          .then(() => {
            dispatch({ type: SEND_MESSAGE_SUCCESS });
          });
      })
      .then(() => {
        firebase.database().ref(`/user_chats/${currentUserEmailB64}/${contactEmailB64}`)
          .set({ name: contactName, email: contactEmail });
      })
      .then(() => {
        firebase.database().ref(`/contacts/${currentUserEmailB64}`)
          .once('value')
          .then((snapshot) => {
            if (snapshot.val()) {
              const currentUserData = _.first(_.values(snapshot.val()));

              firebase.database().ref(`/user_chats/${contactEmailB64}/${currentUserEmailB64}`)
                .set({ name: currentUserData.name, email: currentUser.email });
            }
          });
      });
  };
};

export const userChatFetch = (contactEmail) => {
  const { currentUser } = firebase.auth();
  const currentUserEmailB64 = b64.encode(currentUser.email);
  const contactEmailB64 = b64.encode(contactEmail);

  return (dispatch) => {
    dispatch({
      type: USER_CHAT_LIST_FETCH,
      payload: []
    });

    firebase.database().ref(`/messages/${currentUserEmailB64}/${contactEmailB64}`)
      .on('value', (snapshot) => {
        dispatch({
          type: USER_CHAT_LIST_FETCH,
          payload: snapshot.val()
        });
      });
  };
};
