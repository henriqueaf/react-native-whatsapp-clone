//AuthenticationReducer
export const MODIFY_EMAIL = 'modify_email';
export const MODIFY_PASSWORD = 'modify_password';
export const MODIFY_NAME = 'modify_name';
export const SIGN_UP_USER_SUCCESS = 'sign_up_user_success';
export const SIGN_UP_USER_ERROR = 'sign_up_user_error';
export const AUTHENTICATE_USER_SUCCESS = 'authenticate_user_success';
export const AUTHENTICATE_USER_ERROR = 'authenticate_user_error';
export const LOGIN_IN_PROGRESS = 'login_in_progress';
export const SIGN_UP_IN_PROGRESS = 'sign_up_in_progress';

//AppReducer
export const MODIFY_ADD_CONTACT_EMAIL = 'modify_add_contact_email';
export const ADD_CONTACT_SUCCESS = 'add_contact_success';
export const ADD_CONTACT_ERROR = 'add_contact_error';

//Contacts
export const USER_CONTACTS_LIST = 'user_contacts_list';

//CHAT
export const CHANGE_MESSAGE = 'change_message';
export const SEND_MESSAGE_SUCCESS = 'send_message_success';
export const USER_CHAT_LIST_FETCH = 'user_chat_list_fetch';
