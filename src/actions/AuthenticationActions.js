import firebase from 'firebase';
import b64 from 'base-64';
import { NavigationActions } from 'react-navigation';

import {
  MODIFY_EMAIL,
  MODIFY_PASSWORD,
  MODIFY_NAME,
  SIGN_UP_USER_SUCCESS,
  SIGN_UP_USER_ERROR,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_ERROR,
  LOGIN_IN_PROGRESS,
  SIGN_UP_IN_PROGRESS
} from '../actions/types';

export const modifyEmail = (text) => (
  {
    type: MODIFY_EMAIL,
    payload: text
  }
);

export const modifyPassword = (text) => (
  {
    type: MODIFY_PASSWORD,
    payload: text
  }
);

export const modifyName = (text) => (
  {
    type: MODIFY_NAME,
    payload: text
  }
);

export const signUpUser = ({ name, email, password, navigate }) => {
  return (dispatch) => {
    dispatch({ type: SIGN_UP_IN_PROGRESS });

    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((user) => {
        const emailBase64 = b64.encode(email);

        firebase.database().ref(`/contacts/${emailBase64}`)
          .push({ name })
          .then((value) => signUpUserSuccess(navigate, dispatch));
      })
      .catch((error) => signUpUserError(error, dispatch));
  };
};

const signUpUserSuccess = (navigate, dispatch) => {
  dispatch({
    type: SIGN_UP_USER_SUCCESS
  });

  navigate('Welcome');
};

const signUpUserError = (error, dispatch) => {
  dispatch({
    type: SIGN_UP_USER_ERROR,
    payload: error.message
  });
};

export const authenticateUser = ({ email, password, navigate }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_IN_PROGRESS });

    firebase.auth().signInWithEmailAndPassword(email, password)
      .then((value) => authenticateUserSuccess(navigate, dispatch))
      .catch((error) => authenticateUserError(error, dispatch));
  };
};

const authenticateUserSuccess = (navigate, dispatch) => {
  dispatch({
    type: AUTHENTICATE_USER_SUCCESS
  });

  navigate('Main');
};

const authenticateUserError = (error, dispatch) => {
  dispatch({
    type: AUTHENTICATE_USER_ERROR,
    payload: error.message
  });
};

export const checkUserSignedIn = (navigation) => {
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Main' })
        ]
      });
      navigation.dispatch(resetAction);
    }
  });

  return {
    type: 'none'
  };
};
