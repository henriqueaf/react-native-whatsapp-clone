import {
  MODIFY_ADD_CONTACT_EMAIL,
  ADD_CONTACT_ERROR,
  ADD_CONTACT_SUCCESS,
  CHANGE_MESSAGE,
  SEND_MESSAGE_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  addContactEmail: '',
  addContactResultError: '',
  addContactResultSuccess: false,
  message: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MODIFY_ADD_CONTACT_EMAIL:
      return { ...state, addContactEmail: action.payload };
    case ADD_CONTACT_SUCCESS:
      return {
        ...state,
        addContactResultSuccess: action.payload,
        addContactResultError: '',
        addContactEmail: ''
      };
    case ADD_CONTACT_ERROR:
      return { ...state, addContactResultError: action.payload };
    case CHANGE_MESSAGE:
      return { ...state, message: action.payload };
    case SEND_MESSAGE_SUCCESS:
      return { ...state, message: '' };
    default:
      return state;
  }
};
