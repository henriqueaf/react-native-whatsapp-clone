import {
  MODIFY_EMAIL,
  MODIFY_PASSWORD,
  MODIFY_NAME,
  SIGN_UP_USER_SUCCESS,
  SIGN_UP_USER_ERROR,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_ERROR,
  LOGIN_IN_PROGRESS,
  SIGN_UP_IN_PROGRESS
} from '../actions/types';

const INITIAL_STATE = {
  name: '',
  email: '',
  password: '',
  signUpError: '',
  authenticateError: '',
  loadingLogin: false,
  loadingSignUp: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MODIFY_EMAIL:
      return { ...state, email: action.payload };
    case MODIFY_PASSWORD:
      return { ...state, password: action.payload };
    case MODIFY_NAME:
      return { ...state, name: action.payload };
    case SIGN_UP_USER_SUCCESS:
      return { ...state, name: '', password: '', loadingSignUp: false };
    case SIGN_UP_USER_ERROR:
      return { ...state, signUpError: action.payload, loadingSignUp: false };
    case AUTHENTICATE_USER_SUCCESS:
      return { ...state, authenticateError: '', loadingLogin: false };
    case AUTHENTICATE_USER_ERROR:
      return { ...state, authenticateError: action.payload, loadingLogin: false };
    case LOGIN_IN_PROGRESS:
      return { ...state, loadingLogin: true };
    case SIGN_UP_IN_PROGRESS:
      return { ...state, loadingSignUp: true };
    default:
      return state;
  }
};
