import { combineReducers } from 'redux';

import AuthenticationReducer from './AuthenticationReducer';
import AppReducer from './AppReducer';
import UserContactsListReducer from './UserContactsListReducer';
import ChatListReducer from './ChatListReducer';

export default combineReducers({
  AuthenticationReducer,
  AppReducer,
  UserContactsListReducer,
  ChatListReducer
});
