import {
  USER_CONTACTS_LIST
} from '../actions/types';

const INITIAL_STATE = {
  contactsList: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_CONTACTS_LIST:
      return { ...state, contactsList: action.payload };
    default:
      return state;
  }
};
