import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';

import Navigation from './Navigation';
import reducers from './reducers';

const config = {
  apiKey: 'AIzaSyADN9dUhA2CGBLYXSGlrvdBhkx-KLh1gUc',
  authDomain: 'whatsapp-clone-8293e.firebaseapp.com',
  databaseURL: 'https://whatsapp-clone-8293e.firebaseio.com',
  projectId: 'whatsapp-clone-8293e',
  storageBucket: '',
  messagingSenderId: '554429117302'
};

export default class App extends Component {
  componentWillMount() {
    firebase.initializeApp(config);
  }

  render() {
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
        <Navigation screenProps={firebase} />
      </Provider>
    );
  }
}
